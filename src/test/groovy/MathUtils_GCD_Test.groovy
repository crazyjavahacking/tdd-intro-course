import org.junit.jupiter.api.Test

import static MathUtils.gcd


class MathUtils_GCD_Test {

    @Test
    void 'GCD(x, x)'() {
        assert gcd(3, 3) == 3
    }

    @Test
    void 'GCD of even single-digit numbers'() {
        assert gcd(4, 6) == 2
    }

    @Test
    void 'GCD of even double-digit numbers'() {
        assert gcd(48, 18) == 6
    }
}
