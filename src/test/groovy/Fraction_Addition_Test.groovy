import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource


@DisplayName("Fraction - Addition")
class Fraction_Addition_Test {

    @DisplayName("addition")
    @ParameterizedTest(name = "x|y + 0 ----- {0}/{1} + {2}/{3} = {0}/{1}")
    @CsvSource(["2,3  ,  0,2"])
    void addition(int num1, int den1, int num2, int den2) {
        Fraction fraction1 = new Fraction(num1, den1)
        Fraction fraction2 = new Fraction(num2, den2)

        assert fraction1.plus(fraction2) == fraction1
    }

    @Test
    void 'x|y + 0'() { // = x|y
        Fraction fraction     = new Fraction(2, 3);
        Fraction zeroFraction = new Fraction(0, 2);

        assert fraction.plus(zeroFraction) == fraction
    }

    @Test
    void 'x|y + 1'() { // = x+y|y
        Fraction fraction1 = new Fraction(2, 3);
        Fraction fraction2 = new Fraction(1, 1);

        assert fraction1.plus(fraction2) == new Fraction(5, 3)
    }

    @Test
    void 'x|y + z|y'() { // = x+z|y
        Fraction fraction1 = new Fraction(2, 3);
        Fraction fraction2 = new Fraction(8, 3);

        assert fraction1.plus(fraction2) == new Fraction(10, 3)
    }

    @Test
    void 'x|y + w|z'() {
        Fraction fraction1 = new Fraction(1, 4);
        Fraction fraction2 = new Fraction(1, 3);

        assert fraction1.plus(fraction2) == new Fraction(7, 12)
    }

//TODO when to commit?
//TODO what if I have a failing test and I added one new class and additional failing test for it ?
//TODO when to stop creating new test cases "just in case" the implementation is not fully working for all cases
//TODO how to group tests ?
}
