import org.junit.jupiter.api.Test


class Fraction_Reduction_Test {

    @Test
    void 'reduction of numerator and denominator equal and not 1'() {
        Fraction fraction = new Fraction(3, 3);

        assert fraction == new Fraction(1, 1)
    }

    @Test
    void 'reduction of single-digit fraction'() {
        Fraction fraction = new Fraction(4, 6);

        assert fraction == new Fraction(2, 3)
    }

    @Test
    void 'reduction of double-digit fraction'() {
        Fraction fraction = new Fraction(48, 18);

        assert fraction == new Fraction(8, 3)
    }

    @Test//TODO test for reduction or fraction addition ?
    void 'fraction reduction and final addition reduction'() {
        Fraction fraction1 = new Fraction(4, 6);
        Fraction fraction2 = new Fraction(1, 3);

        assert fraction1.plus(fraction2) == new Fraction(1, 1)
    }
}
