import org.junit.jupiter.api.Test

class Fraction_Equality_Test {

    @Test
    void '-x|-y == x|y'() {
        Fraction fraction = new Fraction(-3, -4);

        assert fraction == new Fraction(3, 4)
    }

    @Test
    void '-x|y == x|-y'() {
        Fraction fraction = new Fraction(-3, 4);

        assert fraction == new Fraction(3, -4)
    }

    @Test
    void 'x|-y == -x|y'() {
        Fraction fraction = new Fraction(3, -4);

        assert fraction == new Fraction(-3, 4)
    }
}

//parameterized test name should contain numbers or mathematical expressions ?
