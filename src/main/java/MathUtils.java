public class MathUtils {
    private MathUtils() {}


    // using Euclidean algorithm: https://en.wikipedia.org/wiki/Greatest_common_divisor
    public static int gcd(int a, int b) {
        if (a == b)
            return a;

        if (b == 0)
            return a;

        return gcd(b, a % b);
    }
}
