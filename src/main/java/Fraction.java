import lombok.EqualsAndHashCode;


@EqualsAndHashCode
class Fraction {
    private final int numerator;
    private final int denominator;


    public Fraction(int numerator, int denominator) {
        final int gcd = MathUtils.gcd(numerator, denominator);

        this.numerator   = numerator   / gcd;
        this.denominator = denominator / gcd;
    }


    public Fraction plus(Fraction otherFraction) {
        if (otherFraction.isZero())
            return this;

        if (otherFraction.isOne())
            return new Fraction(numerator + denominator, denominator);

        if (denominator == otherFraction.denominator)
            return new Fraction(numerator + otherFraction.numerator, denominator);

        return new Fraction(numerator * otherFraction.denominator + otherFraction.numerator * denominator, denominator * otherFraction.denominator);
    }


    @Override
    public String toString() {
        return numerator + "/" + denominator;
    }

    private boolean isOne() {
        return numerator == denominator;
    }

    private boolean isZero() {
        return numerator == 0;
    }
}
